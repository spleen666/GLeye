# Roadmap

## Short-Term

- focusing real-world heuristics for metrics refinement
- switch from 0..100% output to low/medium/good

## Long-Term

- create a reliable swiss-knife for webgl devs :)
