/*  */

module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-collection');    
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        uglify: {

            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> - copyright (c) 2016 Andrea Bovo <spleen666@gmail.com>*/',
                mangle: false,
                compress: {
                    drop_console: true
                }
            },
           
            dist: {
                files: { 'dist/GLeye.min.js': [ "src/GLeye.js" ] }
            }
        }

    });

    grunt.registerTask('dist', ['uglify:dist']);
   
    grunt.registerTask('default', ['dist']);

};
