## GLeye.js

An attempt to provide concise metrics about WebGL support on your device. Main points:

* Offer simple, reliable metrics through JavaScript functions; [WebGL Stats](https://www.webglstats.com/) used as heuristic knowledge-base 
* Working as [UMD](https://github.com/umdjs/umd) Javascript Module (Universal Module Definition).

Actually

* check if unmasked vendor/gpu available (<b>UNMASKED_RENDERER_WEBGL, UNMASKED_VENDOR_WEBGL</b> )
* check if lose/restore context supported (<b>WEBGL_lose_context</b> )
* check if multisample antialiasing supported (<b>SAMPLES, ALIASED_LINE_WIDTH_RANGE, ALIASED_POINT_SIZE_RANGE</b>)
* check [WebGL](https://www.khronos.org/registry/webgl/extensions/) extensions support 
* check shaders support (<b>getShaderPrecisionFormat, MAX_VERTEX_ATTRIBS, MAX_FRAGMENT_UNIFORM_VECTORS, MAX_VERTEX_UNIFORM_VECTORS,MAX_VARYING_VECTORS</b>)
* check texture support (<b>MAX_TEXTURE_IMAGE_UNITS, MAX_TEXTURE_SIZE, MAX_RENDERBUFFER_SIZE, MAX_VIEWPORT_DIMS, MAX_VERTEX_TEXTURE_IMAGE_UNITS, EXT_shader_texture_lod, WEBGL_depth_texture, WEBGL_compressed_texture_s3tc, EXT_texture_filter_anisotropic, OES_texture_float, OES_texture_half_float</b>)

Still in progress; <b>suggestions/feedback are welcome<b>!

## Test page

[GLeye.js test](http://spleennooname.github.io/GLeye/index.html)

## Usage

Include the library in your page.

```html
<script src="GLeye.min.js"></script>

```
Use main method to get a overall metric value:

```javascript
   //get a overall quality value of WebGL support, normalized from 0 to 100% 
   var qualityWebGLSupport = GLeye.liner(); 
    
```

You could specify a object parameter 

```javascript
	//get the quality value of WebGL multisample aliasing support, normalized from 0 to 100% 
	var qualityWebgGLTextureSupport = GLeye.liner({ antialias:true });
	// equivalent:
	var qualityWebgGLTextureSupport = GLeye.liner("antialias");
    
```

## API

* ``` GLeye.liner() ``` return normalized metric

* ``` GLeye.info() ``` return object with WebGL info Context

## References

* [https://www.webglstats.com/](https://www.webglstats.com/)
* [http://stackoverflow.com/questions/22666556/webgl-texture-creation-trouble](http://stackoverflow.com/questions/22666556/webgl-texture-creation-trouble)
* [https://github.com/KhronosGroup/WebGL/tree/master/conformance-suites/1.0.3/conformance](https://github.com/KhronosGroup/WebGL/tree/master/conformance-suites/1.0.3/conformance)

## Credits

* [pyalot](https://twitter.com/pyalot) for [WebGL Stats](https://www.webglstats.com/)

* [greggman](https://github.com/greggman) for feedback/infos


## License

Copyright (c) 2016, Andrea Bovo <spleen666@gmail.com> 
