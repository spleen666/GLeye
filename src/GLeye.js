/*

GLeye.js 

Copyright 2016, Andrea Bovo. <spleen666@gmail.com> All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
in the documentation and/or other materials provided with the distribution.

Neither the name of Andrea Bovo nor the names of his contributors may be used to endorse 
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS 
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY 
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, 
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

@author Andrea Bovo <spleen666@gmail.com>

*/

(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.GLeye = factory();
    }
}(this, function() {

    "use strict";

    //polyfills, etc
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.indexOf(searchString, position) === position;
        };
    }

    // Add indexOf to IE.
    if(!Array.indexOf){
        Array.prototype.indexOf = function(obj){
            for(var i=0; i<this.length; i++){
                if(this[i]==obj){
                    return i;
                }
            }
            return -1;
        };
    }
   
    //thx: https://toddmotto.com/mastering-the-module-pattern/

    var GLeye = (function() {

        var _canvas,

            _gl = null,

            _attrs= null,

            _exts= null,

            _context_info = {},

            _fragment = {},

            _vertex = {},

            _shaders ={},

            _textures = {},

            _antialiasing = {},

            _VENDORS = [
                "WEBKIT_", "MOZ_", "IE_", "O_"
            ],

            _CONTEXT_NAMES = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],

            _FLAGS_STR = "textures antialiasing shaders extensions",

           
            _KHRN_EXTS_OBJ = {

                "OES_texture_float": { type: 0 },

                "OES_texture_half_float": { type: 0 },

                "WEBGL_lose_context": { type: 0 },

                "OES_standard_derivatives": { type: 0 },

                "OES_vertex_array_object": { type: 0 },

                "WEBGL_debug_renderer_info": { type: 0 },

                "WEBGL_debug_shaders": { type: 0 },

                "WEBGL_compressed_texture_s3tc": { type: 0 },

                "WEBGL_depth_texture": { type: 0 },

                "OES_element_index_uint": { type: 0 },

                "EXT_texture_filter_anisotropic": { type: 0 },   
             
                "EXT_frag_depth": { type: 0 },
              
                "WEBGL_draw_buffers": { type: 0 },

                "ANGLE_instanced_arrays": { type: 0 },
                
                "OES_texture_float_linear": { type: 0 },
                
                "OES_texture_half_float_linear": { type: 0 },

                "EXT_blend_minmax": { type: 0 },
               
                "EXT_shader_texture_lod": { type: 0 }

            };

        //utils 

        var _round = function(value, decimals) {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        };

        var _nearest_pow2 = function( aSize ){
          return Math.pow( 2, Math.round( Math.log( aSize ) / Math.log( 2 ) ) ); 
        };
    
        var _norm = function(min, max, val) {
            return (val - min) / (max - min);
        };

        var _ext = function(name){
            var ext = _gl.getExtension(name);
            return _gl.getExtension(name) !== null ? ext : "n/a"; 
        }

        var _prm =  function(name){
            var prm = typeof name === "number" ? _gl.getParameter( name ) : _gl.getParameter(_gl[name]);
            return prm !== null ? prm : "n/a"; 
        }


         /* 
                https://www.khronos.org/registry/webgl/extensions

                WebGL extensions are prefixed with "ANGLE", "OES", "EXT" or "WEBGL". These prefixes reflect origin and intent:
                
                ANGLE_: Extensions that are written by the ANGLE library authors.
                
                OES_: Extensions that mirror functionality from OpenGL ES or OpenGL API extensions approved by the respective architecture review boards.
                
                EXT_: Extensions that mirror other OpenGL ES or OpenGL API extensions.

                WEBGL_: Extensions that are WebGL-specific and intended to be compatible with multiple web browsers. 
                It should also be used for extensions which originated with the OpenGL ES or OpenGL APIs, but whose behavior has been significantly altered
            
            */

        var _test_texture =function( type){
             var tx = _gl.createTexture();

                _gl.bindTexture( _gl.TEXTURE_2D, tx);
                _gl.texImage2D( _gl.TEXTURE_2D, 0, _gl.RGBA, 2, 2, 0, _gl.RGBA, _gl[type], null ); 
                _gl.texParameteri(_gl.TEXTURE_2D, _gl.TEXTURE_MAG_FILTER, _gl.LINEAR);
                _gl.texParameteri(_gl.TEXTURE_2D, _gl.TEXTURE_MIN_FILTER, _gl.LINEAR);

                var framebuffer = _gl.createFramebuffer();
                _gl.bindFramebuffer(_gl.FRAMEBUFFER, framebuffer);
                _gl.framebufferTexture2D(_gl.FRAMEBUFFER, _gl.COLOR_ATTACHMENT0, _gl.TEXTURE_2D, tx, 0 );
              
                var check = _gl.checkFramebufferStatus(_gl.FRAMEBUFFER) != _gl.FRAMEBUFFER_COMPLETE ? true : false;

                _gl.deleteTexture(tx);

                _gl.deleteFramebuffer(framebuffer);
                _gl.bindTexture(_gl.TEXTURE_2D, null);
                _gl.bindFramebuffer(_gl.FRAMEBUFFER, null);

                return check;
        }     

        var _can_render_tofloat =function(){

            var _support_f = false;
            var _support_hf = false;
            var ext;

            var ext1 = _ext("OES_texture_float");
            if (ext1 !== null){
                _support_f = _test_texture("FLOAT");
            }

            var ext2 = _ext("OES_texture_half_float");
            if (ext2 !== null){
                _support_hf = _test_texture("HALF_FLOAT");
            }
           
            return {
                f: _support_f,
                hf: _support_hf
            }
        }


        
        var _vendor_prefix = function() {
            var str = _prm("RENDERER").toLowerCase(),
                vend = "";
            if (str.indexOf("webkit") !== -1) {
                vend = "WEBKIT_";
            } 
            else
            if (str.indexOf("mozilla") !== -1) {
                vend = "MOZ_";
            }

            return vend;
        };
   

        //init

        var _init = function() {    

            var ext, _score=0;
          
            if ( !!window.WebGLRenderingContext ){   

                    try {
            
                        _canvas = document.createElement("canvas");                    

                        for(var n = 0; n < _CONTEXT_NAMES.length; n++){

                            _gl = _canvas.getContext( _CONTEXT_NAMES[n] );  
                            
                            if (_gl !== null && _gl instanceof WebGLRenderingContext ) {
                                
                                //console.log("ERR", _gl.CONTEXT_LOST_WEBGL, _gl.INVALID_OPERATION, _gl.INVALID_FRAMEBUFFER_OPERATION, _gl.INVALID_VALUE, _gl.NO_ERROR)
                             

                                _attrs = _gl.getContextAttributes();    

                                _exts = _gl.getSupportedExtensions();  

                                _context_info = {
                                    contextName : _CONTEXT_NAMES[n] ,
                                    renderer: _prm("RENDERER"),
                                    GLSL: _prm("SHADING_LANGUAGE_VERSION"),
                                    WebGL: _prm("VERSION"),
                                    gpu: "n/a",
                                    vendor: "n/a"
                                };                     
                                
                                /* check if unmasked gpu/vendor available */

                                ext = _ext('WEBGL_debug_renderer_info');
                                if (ext  !== "n/a") {
                                    _context_info.gpu = _prm(ext.UNMASKED_RENDERER_WEBGL);
                                    _context_info.vendor = _prm(ext.UNMASKED_VENDOR_WEBGL);
                                } 

                                /* check if lose context , force trigger */
                                //https://www.khronos.org/webgl/wiki/HandlingContextLost
                               
                                ext = _ext('WEBGL_lose_context');
                                //console.log(ext)
                                if ( ext !== "n/a"){
                                    /*ext.loseContext(); // trigger a context loss
                                    ext.restoreContext(); // restores the context
                                    var err = _gl.getError();
                                    if (err === _gl.CONTEXT_LOST_WEBGL) {
                                       _context_info.contextRestore = false;
                                       _canvas = document.createElement("canvas");
                                       _gl = _canvas.getContext( _CONTEXT_NAMES[n] );
                                    }
                                    else
                                    if (err === _gl.NO_ERROR){
                                        _context_info.contextRestore = true;
                                    }*/
                                    _context_info.supportLoseContext = true;
                                }
                                else{
                                     _context_info.supportLoseContext = false;
                                }
                              
                                
                                /* check if multisampling antialiasing available */
                                var aa = _prm("SAMPLES"),
                                    aa_line_width = _prm("ALIASED_LINE_WIDTH_RANGE"),
                                    aa_point_size = _prm("ALIASED_POINT_SIZE_RANGE");
                                _antialiasing = {
                                        quality: aa,
                                        pointSizeRange : aa_point_size,
                                        lineWidthRange: aa_line_width,
                                }                 

                                /* check shaders */

                                /*check if highp supported */
                                var p_frag = _gl.getShaderPrecisionFormat(_gl.FRAGMENT_SHADER, _gl.HIGH_FLOAT);
                                var p_vert = _gl.getShaderPrecisionFormat(_gl.VERTEX_SHADER, _gl.HIGH_FLOAT);
                                _shaders.highpFragSupported = false;
                                _shaders.highpVertSupported = false;
                                if( p_frag !== null)
                                    _shaders.highpFragSupported =  p_frag.precision !== 0 ? true : false;
                                if (p_vert !== null)
                                    _shaders.highpVertSupported =  p_vert.precision !== 0 ? true : false;

                                //29 4-float components supported by 100% or 221 4-float components supported by 90%.
                                _shaders.maxVertexAttrs = _prm("MAX_VERTEX_ATTRIBS"); 
                                _shaders.maxFragUnif = _prm("MAX_FRAGMENT_UNIFORM_VECTORS");
                                _shaders.maxVertexUnif = _prm("MAX_VERTEX_UNIFORM_VECTORS");
                                _shaders.maxVaryngs=_prm("MAX_VARYING_VECTORS");                                   

                                _textures = {
                                    

                                    maxTextureSize: _prm("MAX_TEXTURE_SIZE"), //safe: 2048x2048

                                    maxCubeSize: _prm("MAX_CUBE_MAP_TEXTURE_SIZE"),

                                    maxRenderbufferSize: _prm("MAX_RENDERBUFFER_SIZE"), //safe: size of a devices display

                                    maxVieportsDims: _prm("MAX_VIEWPORT_DIMS"), //safe: size of a devices display

                                    maxTextureVertexUnits: _prm("MAX_VERTEX_TEXTURE_IMAGE_UNITS"),

                                    maxTextureFragmentUnits: _prm("MAX_TEXTURE_IMAGE_UNITS")
                                  
                                };

                                _textures.supportFloat = _can_render_tofloat();

                                //console.log(  _textures.supportFloat )

                                ext = _ext("EXT_shader_texture_lod");
                                _textures.supportShaderLOD =  ext!==null ?  true : false;

                                ext = _ext("WEBGL_depth_texture");
                                _textures.supportDepth =  ext!==null ?  true : false;
                             
                                ext = _ext('WEBGL_compressed_texture_s3tc');
                                _textures.support_S3TC = ext !== null ?  true : false;
                              
                                ext = _ext('EXT_texture_filter_anisotropic');
                                if (ext !== "n/a"){
                                    _textures.supportAnisotropicFilter =  true
                                    _textures.maxAnisotropic = _prm( ext.MAX_TEXTURE_MAX_ANISOTROPY_EXT);
                                }
                                else{
                                      _textures.supportAnisotropicFilter =  false;
                                      _textures.maxAnisotropic = "n/a";
                                }
                                
                                break; //end for
                            }
                            else{
                                //gl null for _CONTEXT_NAME[n]

                                 // Browser has no idea what WebGL is. Suggest they
                                 // get a new browser by presenting the user with link to
                                 // http://get.webgl.orgd
                            }
                        }

                    } 
                    catch (e) {
                        console.log("GLeye error:", e.message);
                       _gl = null;
                    }         

            }
            else{
               // Browser has no idea what WebGL is. Suggest they
               // get a new browser by presenting the user with link to
                // http://get.webgl.orgd
                _score = 0;
                _gl = null;
            }
        };


         //rating
        
        var _get_rating = function() {

            var p_max = 0,
                p = 0,
                pp = 0,

                _flags = {
                    antialias: true, //enable sniffer for antialiasing
                    textures: true, //enable sniffer for textures
                    shaders: true, //enable sniffer for antialiasing
                    extensions: true //enable sniffer for antialiasing          
                };
                      
            if (!!window.WebGLRenderingContext) {

                    if (arguments.length === 1 && typeof arguments[0] === "string" && _FLAGS_STR.split(" ").indexOf( arguments[0] ) !== -1 ) {
                            _flags ={}; //check 1 flag
                            _flags[ arguments[0] ] = true;  
                    }

                    /* overall */

                    if (_gl !== null && _gl instanceof WebGLRenderingContext ) {

                            p += _attrs.failIfMajorPerformanceCaveat !== null ?  1 : 0;
                            p += _attrs.premultipliedAlpha !== null ?  1 : 0;
                            p += _attrs.stencil  !== null ?  1 : 0;
                            p += _context_info.gpu !== "n/a" ? 1 : 0;
                            p += _context_info.vendor !== "n/a" ? 1 : 0;
                             p += _context_info.supportLoseContext ? 1: 0;

                            p_max += 6;

                           for (var sup in _flags ) {

                                if (sup.indexOf("antialias")!==-1 & _flags[sup] === true) {

                                                           
                                    p += _antialiasing.quality > 0 ? 1 : 0;
                                    p += _antialiasing.lineWidthRange !== "n/a" ? 1 : 0;
                                    p += _antialiasing.pointSizeRange !== "n/a" ? 1 : 0;

                                    p_max += 3;

                                    //console.log( _gl )
                                    //console.log("a.aliasing", p, "of", p_max, _antialiasing)

                                } 
                                else
                                if (sup.indexOf("textures") !== -1 & _flags[sup] === true) {

                                    //textures related
                                    var w = window.screen.availWidth;
                                    var h = window.screen.availHeight;

                                    p += _textures.maxTextureFragmentUnits >= 16 ? 1 : 0;
                                    p += _textures.maxTextureVertexUnits >= 4 ? 1 : 0;

                                    p += _textures.supportDepth ? 1 : 0;

                                    p += _textures.supportFloat.f ? 1 : 0;
                                    p += _textures.supportFloat.hf ? 1 : 0;

                                    p += _textures.maxTextureSize >= 2048 ? 1 : 0;
                                    p += _textures.maxRenderBufferSize >= (w * h) ? 1 : 0;
                                    p += _textures.maxVieportsDims >= (w * h) ? 1 : 0;
                        
                                    p += _textures.support_S3TC ? 1 : 0;                                    
                                    p += _textures.supportAnisotropicFilter ? 1 : 0;
                                    p += _textures.maxAnisotropic !== "n/a" ? 1 : 0;
                                    p += _textures.supportShaderLOD ? 1 : 0;

                                    p_max +=12;

                                } 
                                else
                                if (sup.indexOf("shaders")!==-1 & _flags[sup] === true) {

                                    p += _shaders.highpVertSupported ? 1 : 0;
                                    p += _shaders.highpFragSupported ? 1 : 0;
                                    p += _shaders.maxFragUnif >= 221 || _shaders.maxFragUnif >= 29 ? 1 : 0;
                                    p += _shaders.maxVertexAttrs >= 16 ? 1 : 0;
                                    p += _shaders.maxVertexUnif >= 253 ? 1 : 0;
                                    p += _shaders.maxVaryngs >= 8 ? 1 : 0;

                                    p_max += 6;                        

                                }
                                else
                                if (sup.indexOf("extensions")!==-1 & _flags[sup] === true) {

                                    var name_ext, _pref = _vendor_prefix();
                                    
                                    for ( name_ext in _KHRN_EXTS_OBJ) {
                                            switch (_KHRN_EXTS_OBJ[name_ext].type) {
                                                case 0:
                                                    p_max += 1.0;
                                                break;                                    
                                                case 1:
                                                    p_max += 1.25;
                                                break;
                                            } 
                                    }

                                    for (name_ext in _KHRN_EXTS_OBJ) {
                               
                                        if (_exts.indexOf(name_ext) !== -1) {
                                            
                                            switch (_KHRN_EXTS_OBJ[name_ext].type) {

                                                case 0:
                                                    if (_pref !== "" && _exts.indexOf(_pref + name_ext) !== -1) {
                                                        p += 0;
                                                        //console.log("vendor-specific:", _pref + ext, _KHRN_EXTS_OBJ[ext].type, 0)
                                                    }
                                                    else{
                                                        p += 1.0;
                                                    }

                                                    break;
                                                
                                                case 1:
                                                     p += 1.25;
                                                    break;
                                            } 
                                                                       
                                        }
                                    }
                                }
                            }
                    }                
                }

                pp = _round( _norm(0, p_max, p ) * 100, 2);


                return pp
        };

          //init
        _init();

        /* public api */

        return {
            info: _context_info,
            liner: _get_rating
        };

    })();

    return GLeye;
}));
